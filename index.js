const fs = require('fs')
const readline = require('readline')
const minimist = require('minimist')
const version = require('./package.json').version

let args = minimist(process.argv.slice(2), {
	string: [ 'module', 'combolist', 'proxylist', 'output' ],
	alias: {
		help: 'h',
		version: 'v',
		module: 'm',
		combolist: 'c',
		proxylist: 'p',
		output: 'o'
	},
	default: {
		combolist: 'combo.txt',
		proxylist: 'proxies.txt',
		output: 'output.txt'
	}
});

if (args.help) {
	helpPage()
	process.exit()
}
if (args.version) {
	console.log(version)
	process.exit()
}

if (args.module) {
	if(fs.existsSync(`modules/${args.module.toLowerCase()}.js`) && args.module.toLowerCase() !== 'module') {
		const ModuleClass = require(`./modules/${args.module.toLowerCase()}.js`)
		const module = new ModuleClass()
		const combolist = readline.createInterface({
			input: fs.createReadStream(args.combolist)
		})

		let all = 0
		let working = 0

		combolist.on('line', combo => {
			if(module.check(combo)) {
				working++
			}
			all++
		})

		combolist.on('close', () => {
			console.log(`Checked: ${all}\n` +
				`Working: ${working}\n`)
		})
	} else {
		console.log(`The module '${args.module}' doesn't exist.`)
	}
} else {
	helpPage()
}

function helpPage() {
	// TODO: Sane help page
	console.log('Help page')
}

